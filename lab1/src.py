#!/usr/bin/env python3
from numpy import arange, pi, sin, cos
from matplotlib.pyplot import plot, show, xlabel, ylabel, title, grid, subplot
from matplotlib import rc

rc('font', **{'family': 'serif'})
rc('text', usetex=True)
rc('text.latex', unicode=True)
rc('text.latex', preamble='\\usepackage[utf8]{inputenc}')
rc('text.latex', preamble='\\usepackage[russian]{babel}')

N = 8
Δt = pi / N
t = arange(0.0, N * Δt, Δt)
x = sin(2 * t) + cos(2 * t)


def original():
    subplot(2, 2, 1)
    plot(t, x, 'r.-')
    title('Исходные данные\n x(t) = sin(2t) + cos(2t)')
    xlabel('t')
    ylabel('x(t)')
    grid(True)

if __name__ == '__main__':
    plot(t, x, 'r.-')
    title('Исходные данные\n x(t) = sin(2t) + cos(2t)')
    xlabel('t')
    ylabel('x(t)')
    grid(True)
    show()
