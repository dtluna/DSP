#!/usr/bin/env python3
from numpy import array, exp, pi, angle, sum
from src import N, t, x, original
from matplotlib.pyplot import subplot, show, plot, title, ylabel, xlabel, grid


def dft(x):
    for k in range(N):
        elements = (x[n] * exp(-2 * pi * 1j * k * n / N) for n in range(N))
        yield(sum(elements) / N)


def rdft(x):
    for n in range(N):
        elements = (x[k] * exp(2 * pi * 1j * k * n / N) for k in range(N))
        yield(sum(elements))


def amplitude(y):
    subplot(2, 2, 3)
    amps = list(map(abs, y))
    plot(amps, 'g.-')
    title('Амплитудный спектр')
    xlabel('n')
    # ylabel('\\omega(n)')
    grid(True)


def frequency(y):
    subplot(2, 2, 4)
    freqs = list(map(angle, y))
    plot(freqs, 'c.-')
    title('Частотный спектр')
    xlabel('n')
    ylabel('\\omega(n)')
    grid(True)


def reverse(y):
    subplot(2, 2, 2)
    origs = list(rdft(y))
    plot(t, origs, 'r.-')
    title('Обратное преобразование')
    xlabel('t')
    ylabel('x(t)')
    grid(True)

if __name__ == '__main__':
    original()
    y = list(dft(x))
    amplitude(y)
    frequency(y)
    reverse(y)
    show()
